from infrastructure.dependency_injection_container import DependencyInjectionContainer
from flask import Flask, request, jsonify
import json
import os

def create_app():
    flask_app = Flask(__name__)
    return flask_app

app = create_app()
container = DependencyInjectionContainer()

@app.route('/registration/insurance-applications', methods=['POST'])
def submit_insurance_application():
    usecase = container.submit_insurance_application_usecase()
    params = request.json
    response = usecase.execute(params)
    return jsonify({ 'id': response.id })

@app.route('/registration/insurance-applications/<application_id>/proposal', methods=['GET'])
def get_insurance_plan_proposal(application_id):
    proposal = container.insurance_plan_proposals_repository.find_by_application_id(int(application_id))
    response = jsonify({ 'auto': proposal.auto,
             'home': proposal.home,
             'life': proposal.life,
             'disability': proposal.disability })
    return response

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)

