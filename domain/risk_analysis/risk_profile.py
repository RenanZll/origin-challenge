from domain.base_entity import BaseEntity
from domain.risk_analysis.events.risk_profile_evaluated import RiskProfileEvaluated

class RiskProfile(BaseEntity):
    def __init__(self, insurance_application_id):
        super(RiskProfile, self).__init__()
        self.insurance_application_id = insurance_application_id
        self.life_insurance = None
        self.home_insurance = None
        self.auto_insurance = None
        self.disability_insurance = None
        self.add_creation_domain_event()

    def add_creation_domain_event(self):
        self.domain_events.append(RiskProfileEvaluated(self))
