from domain.risk_analysis.risk_profile import RiskProfile

class RiskProfileEngine:
    def __init__(self, life_rule_engine, disability_rule_engine, home_rule_engine, auto_rule_engine):
        self.life_rule_engine=life_rule_engine
        self.home_rule_engine=home_rule_engine
        self.auto_rule_engine=auto_rule_engine
        self.disability_rule_engine=disability_rule_engine

    def evaluate(self, insurance_application):
        risk_profile = RiskProfile(insurance_application.id)
        risk_profile.life_insurance = self.life_rule_engine.evaluate(insurance_application)
        risk_profile.home_insurance = self.home_rule_engine.evaluate(insurance_application)
        risk_profile.auto_insurance = self.auto_rule_engine.evaluate(insurance_application)
        risk_profile.disability_insurance = self.disability_rule_engine.evaluate(insurance_application)

        return risk_profile

