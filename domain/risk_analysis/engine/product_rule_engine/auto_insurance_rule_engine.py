from domain.risk_analysis.product_risk_profile import ProductRiskProfile

class AutoInsuranceRuleEngine:
    def __init__(self, risk_questions_rule, age_rule, income_rule, vehicle_rule):
        self.age_rule = age_rule
        self.income_rule = income_rule
        self.vehicle_rule = vehicle_rule
        self.risk_questions_rule = risk_questions_rule

    def evaluate(self, application):
        score = 0
        eligibility = self.__is_eligible(application)
        if eligibility: score = sum(map(lambda rule: rule.calculate(application), self.__rules()))
        return self.__build_product_profile(eligibility, score)

    def __is_eligible(self, application):
        vehicle = application.vehicle
        return vehicle is not None

    def __rules(self):
        return [self.risk_questions_rule,
                self.age_rule,
                self.income_rule,
                self.vehicle_rule]

    def __build_product_profile(self, eligibility, score):
        return ProductRiskProfile(self.__product, eligibility, score)

    def __product(self):
         return "auto_insurance"
