from datetime import datetime

class VehicleRule:
    def __init__(self):
        pass

    def calculate(self, application):
        vehicle = application.vehicle
        if self.__years_since(vehicle.year) < 5: return 1
        return 0

    def __years_since(self, vehicle_year):
        now = datetime.now()
        return now.year - vehicle_year
