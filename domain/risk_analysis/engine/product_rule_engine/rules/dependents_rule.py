class DependentsRule:
    def __init__(self):
        pass

    def calculate(self, application):
        user = application.user
        if user.dependents > 0: return 1
        return 0
