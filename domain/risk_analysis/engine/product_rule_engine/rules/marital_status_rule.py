class MaritalStatusRule:
    def __init__(self):
        pass

    def calculate(self, application):
        user = application.user
        if user.marital_status == "married": return 1
        return 0
