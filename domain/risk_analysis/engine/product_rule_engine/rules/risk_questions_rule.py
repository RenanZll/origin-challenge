class RiskQuestionsRule:
    def __init__(self):
        pass

    def calculate(self, application):
        return sum(application.risk_questions)
