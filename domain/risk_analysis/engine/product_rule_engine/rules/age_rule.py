class AgeRule:
    def __init__(self):
        pass

    def calculate(self, application):
        user = application.user
        if user.age < 30: return -2
        if 30 <= user.age < 40: return -1
        return 0
