class IncomeRule:
    def __init__(self):
        pass

    def calculate(self, application):
        user = application.user
        if user.income > 200000: return -1
        return 0
