class MortgageRule:
    def __init__(self):
        pass

    def calculate(self, application):
        house = application.house
        if house is None: return 0
        if house.ownership_status == "mortgaged": return 1
        return 0
