from domain.risk_analysis.product_risk_profile import ProductRiskProfile

class HomeInsuranceRuleEngine:
    def __init__(self, risk_question_rule, age_rule, income_rule, mortgage_rule):
        self.age_rule = age_rule
        self.income_rule = income_rule
        self.mortgage_rule = mortgage_rule
        self.risk_question_rule = risk_question_rule

    def evaluate(self, application):
        score = 0
        eligibility = self.__is_eligible(application)
        if eligibility: score = sum(map(lambda rule: rule.calculate(application), self.__rules()))
        return self.__build_product_profile(eligibility, score)

    def __is_eligible(self, application):
        house = application.house
        return house is not None

    def __rules(self):
        return [self.risk_question_rule,
                self.age_rule,
                self.income_rule,
                self.mortgage_rule]

    def __build_product_profile(self, eligibility, score):
        return ProductRiskProfile(self.__product, eligibility, score)

    def __product(self):
        return "home_insurance"



