from domain.risk_analysis.product_risk_profile import ProductRiskProfile

class LifeInsuranceRuleEngine:
    def __init__(self, risk_question_rule, age_rule, income_rule, depedents_rule, marital_status_rule):
        self.age_rule = age_rule
        self.income_rule = income_rule
        self.depedents_rule = depedents_rule
        self.marital_status_rule = marital_status_rule
        self.risk_question_rule = risk_question_rule

    def evaluate(self, application):
        score = 0
        eligibility = self.__is_eligible(application)
        if eligibility: score = sum(map(lambda rule: rule.calculate(application), self.__rules()))
        return self.__build_product_profile(eligibility, score)

    def __is_eligible(self, application):
        user = application.user
        return user.age <= 60

    def __rules(self):
        return [self.risk_question_rule,
                self.age_rule,
                self.income_rule,
                self.depedents_rule,
                self.marital_status_rule]

    def __build_product_profile(self, eligibility, score):
        return ProductRiskProfile(self.__product, eligibility, score)

    def __product(self):
        return "life_insurance"



