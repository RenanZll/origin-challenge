class BaseEntity:
    def __init__(self):
        self.id = None
        self.domain_events = []

    def raise_events(self):
        events_to_raise = self.domain_events
        self.domain_events = []
        return events_to_raise
