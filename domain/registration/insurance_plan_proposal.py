from domain.base_entity import BaseEntity

class InsurancePlanProposal(BaseEntity):
    def __init__(self, insurance_application_id, auto, home, disability, life):
        super(InsurancePlanProposal, self).__init__()
        self.insurance_application_id = insurance_application_id
        self.auto = auto
        self.home = home
        self.disability = disability
        self.life = life
