from domain.base_entity import BaseEntity
from domain.registration.events.insurance_application_created  import InsuranceApplicationCreated

class InsuranceApplication(BaseEntity):
    def __init__(self, id = None, user = None, house = None, risk_questions = None, vehicle = None):
        super(InsuranceApplication, self).__init__()
        self.risk_questions = risk_questions
        self.user = user
        self.house = house
        self.vehicle = vehicle
        self.add_creation_domain_event()

    def add_creation_domain_event(self):
        self.domain_events.append(InsuranceApplicationCreated(self))
