from domain.registration.insurance_plan_proposal import InsurancePlanProposal

class InsurancePlanProposalFactory:
    def create(self, risk_profile):
        return InsurancePlanProposal(
                insurance_application_id = risk_profile.insurance_application_id,
                auto = self.recommended_plan(risk_profile.auto_insurance),
                home = self.recommended_plan(risk_profile.home_insurance),
                life = self.recommended_plan(risk_profile.life_insurance),
                disability = self.recommended_plan(risk_profile.disability_insurance))

    def recommended_plan(self, product_risk_profile):
        if product_risk_profile.eligibility == False: return 'ineligible'
        if product_risk_profile.score <= 0: return 'economic'
        if 1 <= product_risk_profile.score <= 2: return 'regular'
        if product_risk_profile.score >= 3: return 'responsible'
