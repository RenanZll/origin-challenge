class EvaluateRiskProfileHandler:
    def __init__(self, evaluate_risk_profile_usecase):
        self.evaluate_risk_profile_usecase = evaluate_risk_profile_usecase

    def handle(self, insurance_application_created):
        self.evaluate_risk_profile_usecase.execute(insurance_application_created.insurance_application)
