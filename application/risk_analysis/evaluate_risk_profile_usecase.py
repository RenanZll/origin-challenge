class EvaluateRiskProfileUsecase:
    def __init__(self, risk_profile_engine, risk_profile_repository):
        self.risk_profile_engine = risk_profile_engine
        self.risk_profile_repository = risk_profile_repository

    def execute(self, insurance_application):
       risk_profile = self.risk_profile_engine.evaluate(insurance_application)
       return self.risk_profile_repository.persist(risk_profile)
