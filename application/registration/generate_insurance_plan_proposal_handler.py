class GenerateInsurancePlanProposalHandler:
    def __init__(self, insurance_plan_proposal_repository, insurance_plan_proposal_factory):
        self.insurance_plan_proposal_repository = insurance_plan_proposal_repository
        self.insurance_plan_proposal_factory = insurance_plan_proposal_factory

    def handle(self, risk_profile_evaluated):
        insurance_plan_proposal = self.insurance_plan_proposal_factory.create(risk_profile_evaluated.risk_profile)
        return self.insurance_plan_proposal_repository.persist(insurance_plan_proposal)


