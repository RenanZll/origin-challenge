from domain.registration.insurance_application import InsuranceApplication
from domain.registration.user import User
from domain.registration.vehicle import Vehicle
from domain.registration.house import House

class SubmitInsuranceApplicationUsecase:
    def __init__(self, repository):
        self.repository = repository

    def execute(self, params):
        application = InsuranceApplication(
                user = User(
                    age = params['age'],
                    dependents = params['dependents'],
                    income = params['income'],
                    marital_status = params['marital_status']
                ),
                house = self.__build_house(params),
                risk_questions = params['risk_questions'],
                vehicle = self.__build_vehicle(params)
                )
        return self.repository.persist(application)

    def __build_house(self, params):
        if params['house'] is None: return None
        return House(ownership_status = params['house']['ownership_status'])

    def __build_vehicle(self, params):
        if params['vehicle'] is None: return None
        return Vehicle(params['vehicle']['year'])
