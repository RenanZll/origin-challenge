from unittest import TestCase
from infrastructure.dependency_injection_container import DependencyInjectionContainer

class IntegrationTestCase(TestCase):
    def __init__(self, *args, **kwargs):
        super(IntegrationTestCase, self).__init__(*args, **kwargs)
        self.di_container = DependencyInjectionContainer()
        self.repository = self.di_container.risk_profiles_repository
        self.usecase = self.di_container.evaluate_risk_profile_usecase()

