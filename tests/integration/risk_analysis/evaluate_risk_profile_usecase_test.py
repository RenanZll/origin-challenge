from tests.integration_test_case import IntegrationTestCase
from domain.registration.insurance_application import InsuranceApplication
from domain.registration.user import User
from domain.registration.vehicle import Vehicle
from domain.registration.house import House

class EvaluateRiskProfileUsecaseTest(IntegrationTestCase):
    def test_usecase_persists_risk_profile_evaluated_for_insurance_application(self):
        usecase = self.di_container.evaluate_risk_profile_usecase()
        repository = self.di_container.risk_profiles_repository
        application = InsuranceApplication(
                id = 123,
                user = User(
                    age = 26,
                    dependents = 0,
                    income = 5000,
                    marital_status = 'single'
                ),
                house = None,
                risk_questions = [0, 1, 0],
                vehicle = None
                )

        usecase.execute(application)

        risk_profile = repository.last()
        self.assertTrue(risk_profile == repository.last())

    def test_usecase_return_risk_profile_with__current_products_eligibity(self):
        usecase = self.di_container.evaluate_risk_profile_usecase()
        application = InsuranceApplication(
                id = 123,
                user = User(
                    age = 26,
                    dependents = 0,
                    income = 5000,
                    marital_status = 'single'
                ),
                house = None,
                risk_questions = [0, 1, 0],
                vehicle = None
                )

        risk_profile = usecase.execute(application)

        self.assertTrue(risk_profile.insurance_application_id == application.id)
        self.assertTrue(risk_profile.disability_insurance.eligibility)
        self.assertTrue(risk_profile.life_insurance.eligibility)
        self.assertFalse(risk_profile.home_insurance.eligibility)
        self.assertFalse(risk_profile.auto_insurance.eligibility)
