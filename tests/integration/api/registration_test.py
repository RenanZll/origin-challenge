from unittest import TestCase
from app import app
import json

class RegistrationAPiTest(TestCase):
    def test_when_posting_a_insurance_application_it_generates_a_insurance_plan_proposal(self):
        application_response = app.test_client().post(
                '/registration/insurance-applications',
                data = self.__application_request_body(),
                content_type='application/json'
        )

        application_response_body = json.loads(application_response.get_data(as_text=True))

        proposal_response = app.test_client().get(
                "/registration/insurance-applications/%s/proposal" % application_response_body['id'],
                content_type='application/json'
        )

        proposal_response_body = json.loads(proposal_response.get_data(as_text=True))

        self.assertTrue(proposal_response.status_code == 200)
        self.assertDictEqual(proposal_response_body, self.__expected_proposal_response())

    def __expected_proposal_response(self):
        return {
            "auto": "regular",
            "disability": "ineligible",
            "home": "economic",
            "life": "regular"
        }

    def __application_request_body(self):
        return json.dumps({
                  "age": 35,
                  "dependents": 2,
                  "house": {"ownership_status": "owned"},
                  "income": 0,
                  "marital_status": "married",
                  "risk_questions": [0, 1, 0],
                  "vehicle": {"year": 2018}
                })
