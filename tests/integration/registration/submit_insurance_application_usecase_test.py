from tests.integration_test_case import IntegrationTestCase

class SubmitInsuranceApplicationUsecaseTest(IntegrationTestCase):
    def test_usecase_persists_insurance_application_builded_from_params(self):
        usecase = self.di_container.submit_insurance_application_usecase()
        repository = self.di_container.insurance_applications_repository
        params = {
          "age": 35,
          "dependents": 2,
          "house": {"ownership_status": "owned"},
          "income": 0,
          "marital_status": "married",
          "risk_questions": [0, 1, 0],
          "vehicle": {"year": 2018}
        }

        application = usecase.execute(params)

        risk_profile = repository.last()
        self.assertTrue(risk_profile == repository.last())

    def test_usecase_builds_the_house_when_there_is_house_params(self):
        usecase = self.di_container.submit_insurance_application_usecase()
        params = {
          "age": 35,
          "dependents": 2,
          "house": {"ownership_status": "owned"},
          "income": 0,
          "marital_status": "married",
          "risk_questions": [0, 1, 0],
          "vehicle": {"year": 2018}
        }

        application = usecase.execute(params)

        self.assertTrue(application.house.ownership_status == "owned")

    def test_usecase_does_not_build_the_house_when_there_is_no_house_params(self):
        usecase = self.di_container.submit_insurance_application_usecase()
        params = {
          "age": 35,
          "dependents": 2,
          "house": None,
          "income": 0,
          "marital_status": "married",
          "risk_questions": [0, 1, 0],
          "vehicle": {"year": 2018}
        }

        application = usecase.execute(params)

        self.assertTrue(application.house is None)

    def test_usecase_builds_the_vehicle_when_there_is_vehicle_params(self):
        usecase = self.di_container.submit_insurance_application_usecase()
        params = {
          "age": 35,
          "dependents": 2,
          "house": {"ownership_status": "owned"},
          "income": 0,
          "marital_status": "married",
          "risk_questions": [0, 1, 0],
          "vehicle": {"year": 2018}
        }

        application = usecase.execute(params)

        self.assertTrue(application.vehicle.year == 2018)

    def test_usecase_does_not_build_the_vehicle_when_there_is_no_vehicle_params(self):
        usecase = self.di_container.submit_insurance_application_usecase()
        params = {
          "age": 35,
          "dependents": 2,
          "house": {"ownership_status": "owned"},
          "income": 0,
          "marital_status": "married",
          "risk_questions": [0, 1, 0],
          "vehicle": None
        }

        application = usecase.execute(params)

        self.assertTrue(application.vehicle is None)
