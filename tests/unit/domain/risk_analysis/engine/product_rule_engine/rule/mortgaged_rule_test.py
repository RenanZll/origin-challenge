from domain.registration.insurance_application import InsuranceApplication
from domain.registration.house import House
from domain.risk_analysis.engine.product_rule_engine.rules.mortgage_rule import MortgageRule
from unittest.mock import Mock

def test_return_1_when_house_is_mortgaged():
    house = Mock(spec=House, ownership_status="mortgaged")
    application = Mock(spec=InsuranceApplication, house=house)

    assert(MortgageRule().calculate(application) == 1)

def test_return_0_when_house_is_owned():
    house = Mock(spec=House, ownership_status="owner")
    application = Mock(spec=InsuranceApplication, house=house)

    assert(MortgageRule().calculate(application) == 0)
