from domain.registration.insurance_application import InsuranceApplication
from domain.registration.user import User
from domain.risk_analysis.engine.product_rule_engine.rules.marital_status_rule import MaritalStatusRule
from unittest.mock import Mock

def test_return_0_when_user_is_single():
    user = Mock(spec=User, marital_status="single")
    application = Mock(spec=InsuranceApplication, user=user)

    assert(MaritalStatusRule().calculate(application) == 0)

def test_return_minus_1_when_user_is_married():
    user = Mock(spec=User, marital_status="married")
    application = Mock(spec=InsuranceApplication, user=user)

    assert(MaritalStatusRule().calculate(application) == 1)
