from domain.registration.insurance_application import InsuranceApplication
from domain.registration.vehicle import Vehicle
from domain.risk_analysis.engine.product_rule_engine.rules.vehicle_rule import VehicleRule
from unittest.mock import Mock
from datetime import datetime

def test_return_1_when_was_produced_in_the_last_5_years():
    now = datetime.now()
    vehicle = Mock(spec=Vehicle, year=now.year - 3)
    application = Mock(spec=InsuranceApplication, vehicle=vehicle)

    assert(VehicleRule().calculate(application) == 1)

def test_return_0_when_was_not_produced_in_the_last_5_years():
    now = datetime.now()
    vehicle = Mock(spec=Vehicle, year=now.year - 8)
    application = Mock(spec=InsuranceApplication, vehicle=vehicle)

    assert(VehicleRule().calculate(application) == 0)
