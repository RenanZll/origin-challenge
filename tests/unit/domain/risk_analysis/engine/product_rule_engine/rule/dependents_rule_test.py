from domain.registration.insurance_application import InsuranceApplication
from domain.registration.user import User
from domain.risk_analysis.engine.product_rule_engine.rules.dependents_rule import DependentsRule
from unittest.mock import Mock

def test_return_0_when_user_has_no_dependents():
    user = Mock(spec=User, dependents=0)
    application = Mock(spec=InsuranceApplication, user=user)

    assert(DependentsRule().calculate(application) == 0)

def test_return_1_when_income_over_200000():
    user = Mock(spec=User, dependents=1)
    application = Mock(spec=InsuranceApplication, user=user)

    assert(DependentsRule().calculate(application) == 1)
