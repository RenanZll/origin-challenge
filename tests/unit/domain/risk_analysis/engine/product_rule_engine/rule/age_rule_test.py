from domain.registration.insurance_application import InsuranceApplication
from domain.registration.user import User
from domain.risk_analysis.engine.product_rule_engine.rules.age_rule import AgeRule
from unittest.mock import Mock

def test_return_minus_2_when_age_under_30():
    user = Mock(User, age=29)
    application = Mock(spec=InsuranceApplication, user=user)

    assert(AgeRule().calculate(application) == -2)

def test_return_minus_1_when_age_between_30_and_40():
    user = Mock(User, age=35)
    application = Mock(spec=InsuranceApplication, user=user)

    assert(AgeRule().calculate(application) == -1)

def test_return_0_when_age_over_40():
    user = Mock(User, age=45)
    application = Mock(spec=InsuranceApplication, user=user)

    assert(AgeRule().calculate(application) == 0)
