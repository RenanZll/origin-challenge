from domain.registration.insurance_application import InsuranceApplication
from domain.risk_analysis.engine.product_rule_engine.rules.risk_questions_rule import RiskQuestionsRule
from unittest.mock import Mock

def test_return_the_sum_of_the_questions_answers():
    application = Mock(spec=InsuranceApplication, risk_questions=[1,1,1])

    assert(RiskQuestionsRule().calculate(application) == 3)
