from domain.registration.insurance_application import InsuranceApplication
from domain.registration.user import User
from domain.risk_analysis.engine.product_rule_engine.rules.income_rule import IncomeRule
from unittest.mock import Mock

def test_return_0_when_income_under_200000():
    user = Mock(spec=User, income=199999)
    application = Mock(spec=InsuranceApplication, user=user)

    assert(IncomeRule().calculate(application) == 0)

def test_return_minus_1_when_income_over_200000():
    user = Mock(spec=User, income=200001)
    application = Mock(spec=InsuranceApplication, user=user)

    assert(IncomeRule().calculate(application) == -1)
