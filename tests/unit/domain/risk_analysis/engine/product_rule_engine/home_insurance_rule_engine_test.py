from domain.registration.insurance_application import InsuranceApplication
from domain.registration.house import House
from domain.risk_analysis.engine.product_rule_engine.rules.risk_questions_rule import RiskQuestionsRule
from domain.risk_analysis.engine.product_rule_engine.rules.age_rule import AgeRule
from domain.risk_analysis.engine.product_rule_engine.rules.income_rule import IncomeRule
from domain.risk_analysis.engine.product_rule_engine.rules.mortgage_rule import MortgageRule
from domain.risk_analysis.engine.product_rule_engine.home_insurance_rule_engine import HomeInsuranceRuleEngine
from unittest.mock import Mock
from unittest import TestCase

class HomeInsuranceRuleEngineTest(TestCase):
    def test_return_eligible_product_profile_if_house_is_present(self):
        house = Mock(spec=House)
        application = Mock(spec=InsuranceApplication, house=house)

        rule_engine = HomeInsuranceRuleEngine(*self.__rules())
        product_profile = rule_engine.evaluate(application)

        self.assertTrue(product_profile.eligibility)

    def test_return_product_profile_with_score_as_sum_of_rules_result(self):
        house = Mock(spec=House)
        application = Mock(spec=InsuranceApplication, house=house)

        rule_engine = HomeInsuranceRuleEngine(*self.__rules())
        product_profile = rule_engine.evaluate(application)

        self.assertTrue(product_profile.score == 4)

    def test_return_ineligible_product_profile_if_vehicle_is_not_present(self):
        application = Mock(spec=InsuranceApplication, house=None)

        rule_engine = HomeInsuranceRuleEngine(*self.__rules())
        product_profile = rule_engine.evaluate(application)

        self.assertFalse(product_profile.eligibility)

    def __rules(self):
        calculate = Mock(return_value=1)
        return [
                Mock(spec=RiskQuestionsRule, calculate=calculate),
                Mock(spec=AgeRule, calculate=calculate),
                Mock(spec=IncomeRule, calculate=calculate),
                Mock(spec=MortgageRule, calculate=calculate)
                ]
