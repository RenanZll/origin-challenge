from domain.registration.insurance_application import InsuranceApplication
from domain.registration.user import User
from domain.risk_analysis.engine.product_rule_engine.rules.risk_questions_rule import RiskQuestionsRule
from domain.risk_analysis.engine.product_rule_engine.rules.age_rule import AgeRule
from domain.risk_analysis.engine.product_rule_engine.rules.income_rule import IncomeRule
from domain.risk_analysis.engine.product_rule_engine.rules.dependents_rule import DependentsRule
from domain.risk_analysis.engine.product_rule_engine.rules.marital_status_rule import MaritalStatusRule
from domain.risk_analysis.engine.product_rule_engine.rules.mortgage_rule import MortgageRule
from domain.risk_analysis.engine.product_rule_engine.disability_insurance_rule_engine import DisabilityInsuranceRuleEngine
from unittest.mock import Mock
from unittest import TestCase

class DisabilityInsuranceRuleEngineTest(TestCase):
    def test_return_eligible_product_profile_if_user_is_60_years_old_or_younger_and_has_income(self):
        user = Mock(spec=User, age=60, income=10000)
        application = Mock(spec=InsuranceApplication, user=user)

        rule_engine = DisabilityInsuranceRuleEngine(*self.__rules())
        product_profile = rule_engine.evaluate(application)

        self.assertTrue(product_profile.eligibility)

    def test_return_product_profile_with_score_as_sum_of_rules_result(self):
        user = Mock(spec=User, age=60, income=10000)
        application = Mock(spec=InsuranceApplication, user=user)

        rule_engine = DisabilityInsuranceRuleEngine(*self.__rules())
        product_profile = rule_engine.evaluate(application)

        self.assertTrue(product_profile.score == 6)

    def test_return_ineligible_product_profile_if_user_is_older_than_60_years_old(self):
        user = Mock(spec=User, age=61, income=10000)
        application = Mock(spec=InsuranceApplication, user=user)

        rule_engine = DisabilityInsuranceRuleEngine(*self.__rules())
        product_profile = rule_engine.evaluate(application)

        self.assertFalse(product_profile.eligibility)

    def test_return_ineligible_product_profile_if_user_has_no_income(self):
        user = Mock(spec=User, age=60, income=0)
        application = Mock(spec=InsuranceApplication, user=user)

        rule_engine = DisabilityInsuranceRuleEngine(*self.__rules())
        product_profile = rule_engine.evaluate(application)

        self.assertFalse(product_profile.eligibility)

    def __rules(self):
        calculate = Mock(return_value=1)
        return [
                Mock(spec=RiskQuestionsRule, calculate=calculate),
                Mock(spec=AgeRule, calculate=calculate),
                Mock(spec=IncomeRule, calculate=calculate),
                Mock(spec=DependentsRule, calculate=calculate),
                Mock(spec=MaritalStatusRule, calculate=calculate),
                Mock(spec=MortgageRule, calculate=calculate)
                ]
