from domain.registration.insurance_application import InsuranceApplication
from domain.risk_analysis.engine.product_rule_engine.auto_insurance_rule_engine import AutoInsuranceRuleEngine
from domain.risk_analysis.engine.product_rule_engine.home_insurance_rule_engine import HomeInsuranceRuleEngine
from domain.risk_analysis.engine.product_rule_engine.disability_insurance_rule_engine import DisabilityInsuranceRuleEngine
from domain.risk_analysis.engine.product_rule_engine.life_insurance_rule_engine import LifeInsuranceRuleEngine
from domain.risk_analysis.engine.risk_profile_engine import RiskProfileEngine
from domain.risk_analysis.product_risk_profile import ProductRiskProfile
from unittest.mock import Mock
from unittest import TestCase

class RiskProfileEngineTest(TestCase):
    def test_return_a_builded_risk_profile_entity_with_all_product_profiles(self):
        application = Mock(spec=InsuranceApplication, id=123)
        home_profile = Mock(spec=ProductRiskProfile, name='home_insurance')
        home_rule_engine = Mock(spec=HomeInsuranceRuleEngine, evaluate=Mock(return_value=home_profile))
        auto_profile = Mock(spec=ProductRiskProfile, name='auto_insurance')
        auto_rule_engine = Mock(spec=HomeInsuranceRuleEngine, evaluate=Mock(return_value=auto_profile))
        life_profile = Mock(spec=ProductRiskProfile, name='life_insurance')
        life_rule_engine = Mock(spec=LifeInsuranceRuleEngine, evaluate=Mock(return_value=life_profile))
        disability_profile = Mock(spec=ProductRiskProfile, name='disability_insurance')
        disability_rule_engine = Mock(spec=DisabilityInsuranceRuleEngine,
                evaluate=Mock(return_value=disability_profile))

        engine = RiskProfileEngine(life_rule_engine, disability_rule_engine, home_rule_engine, auto_rule_engine)
        risk_profile = engine.evaluate(application)

        self.assertTrue(risk_profile.insurance_application_id == application.id)
        self.assertTrue(risk_profile.life_insurance == life_profile)
        self.assertTrue(risk_profile.home_insurance == home_profile)
        self.assertTrue(risk_profile.auto_insurance == auto_profile)
        self.assertTrue(risk_profile.disability_insurance == disability_profile)


