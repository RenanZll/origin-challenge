from domain.registration.insurance_plan_proposal_factory import InsurancePlanProposalFactory
from domain.risk_analysis.engine.risk_profile_engine import RiskProfileEngine
from domain.risk_analysis.product_risk_profile import ProductRiskProfile
from domain.risk_analysis.risk_profile import RiskProfile
from unittest.mock import Mock
from unittest import TestCase

class InsurancePlanProposalFactoryTest(TestCase):
    def test_ineligible_plan_status_is_set_when_risk_profile_returns_not_eligible(self):
        auto_profile = Mock(spec=ProductRiskProfile, name='auto_insurance', eligibility = False)
        home_profile = Mock(spec=ProductRiskProfile, name='home_insurance', eligibility = False)
        life_profile = Mock(spec=ProductRiskProfile, name='life_insurance', eligibility = False)
        disability_profile = Mock(spec=ProductRiskProfile, name='disability_insurance', eligibility = False)
        risk_profile = Mock(spec=RiskProfile,
                id=123,
                insurance_application_id=234,
                auto_insurance=auto_profile,
                home_insurance=home_profile,
                life_insurance=life_profile,
                disability_insurance=disability_profile)

        factory = InsurancePlanProposalFactory()
        proposal = factory.create(risk_profile)

        self.assertTrue(proposal.auto == 'ineligible')
        self.assertTrue(proposal.home == 'ineligible')
        self.assertTrue(proposal.life == 'ineligible')
        self.assertTrue(proposal.disability == 'ineligible')

    def test_economic_plan_status_is_set_when_risk_profile_returns_eligible_and_score_is_lesser_or_equals_0(self):
        auto_profile = Mock(spec=ProductRiskProfile, name='auto_insurance', eligibility = True, score = 0)
        home_profile = Mock(spec=ProductRiskProfile, name='home_insurance', eligibility = True, score = -1)
        life_profile = Mock(spec=ProductRiskProfile, name='life_insurance', eligibility = True, score = -2)
        disability_profile = Mock(spec=ProductRiskProfile, name='disability_insurance', eligibility = True, score = -3)
        risk_profile = Mock(spec=RiskProfile,
                id=123,
                insurance_application_id=234,
                auto_insurance=auto_profile,
                home_insurance=home_profile,
                life_insurance=life_profile,
                disability_insurance=disability_profile)

        factory = InsurancePlanProposalFactory()
        proposal = factory.create(risk_profile)

        self.assertTrue(proposal.auto == 'economic')
        self.assertTrue(proposal.home == 'economic')
        self.assertTrue(proposal.life == 'economic')
        self.assertTrue(proposal.disability == 'economic')


    def test_regular_plan_status_is_set_when_risk_profile_returns_eligible_and_score_is_between_1_and_2(self):
        auto_profile = Mock(spec=ProductRiskProfile, name='auto_insurance', eligibility = True, score = 1)
        home_profile = Mock(spec=ProductRiskProfile, name='home_insurance', eligibility = True, score = 2)
        life_profile = Mock(spec=ProductRiskProfile, name='life_insurance', eligibility = True, score = 1)
        disability_profile = Mock(spec=ProductRiskProfile, name='disability_insurance', eligibility = True, score = 2)
        risk_profile = Mock(spec=RiskProfile,
                id=123,
                insurance_application_id=234,
                auto_insurance=auto_profile,
                home_insurance=home_profile,
                life_insurance=life_profile,
                disability_insurance=disability_profile)

        factory = InsurancePlanProposalFactory()
        proposal = factory.create(risk_profile)

        self.assertTrue(proposal.auto == 'regular')
        self.assertTrue(proposal.home == 'regular')
        self.assertTrue(proposal.life == 'regular')
        self.assertTrue(proposal.disability == 'regular')

    def test_responsible_plan_status_is_set_when_risk_profile_returns_eligible_score_is_greater_or_equals_than_3(self):
        auto_profile = Mock(spec=ProductRiskProfile, name='auto_insurance', eligibility = True, score = 3)
        home_profile = Mock(spec=ProductRiskProfile, name='home_insurance', eligibility = True, score = 4)
        life_profile = Mock(spec=ProductRiskProfile, name='life_insurance', eligibility = True, score = 5)
        disability_profile = Mock(spec=ProductRiskProfile, name='disability_insurance', eligibility = True, score = 6)
        risk_profile = Mock(spec=RiskProfile,
                id=123,
                insurance_application_id=234,
                auto_insurance=auto_profile,
                home_insurance=home_profile,
                life_insurance=life_profile,
                disability_insurance=disability_profile)

        factory = InsurancePlanProposalFactory()
        proposal = factory.create(risk_profile)

        self.assertTrue(proposal.auto == 'responsible')
        self.assertTrue(proposal.home == 'responsible')
        self.assertTrue(proposal.life == 'responsible')
        self.assertTrue(proposal.disability == 'responsible')
