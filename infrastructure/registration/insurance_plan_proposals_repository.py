from infrastructure.base_repository import BaseRepository

class InsurancePlanProposalsRepository(BaseRepository):
    def find_by_application_id(self, application_id):
        for proposal in self.database:
            if proposal.insurance_application_id == int(application_id): return proposal
