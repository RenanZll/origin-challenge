from application.registration.generate_insurance_plan_proposal_handler import GenerateInsurancePlanProposalHandler
from application.registration.submit_insurance_application_usecase import SubmitInsuranceApplicationUsecase
from application.risk_analysis.evaluate_risk_profile_usecase import EvaluateRiskProfileUsecase
from application.risk_analysis.evaluate_risk_profile_handler import EvaluateRiskProfileHandler
from domain.registration.events.insurance_application_created import InsuranceApplicationCreated
from domain.registration.insurance_plan_proposal_factory import InsurancePlanProposalFactory
from domain.risk_analysis.engine.product_rule_engine.auto_insurance_rule_engine import AutoInsuranceRuleEngine
from domain.risk_analysis.engine.product_rule_engine.disability_insurance_rule_engine import DisabilityInsuranceRuleEngine
from domain.risk_analysis.engine.product_rule_engine.home_insurance_rule_engine import HomeInsuranceRuleEngine
from domain.risk_analysis.engine.product_rule_engine.life_insurance_rule_engine import LifeInsuranceRuleEngine
from domain.risk_analysis.engine.product_rule_engine.rules.age_rule import AgeRule
from domain.risk_analysis.engine.product_rule_engine.rules.dependents_rule import DependentsRule
from domain.risk_analysis.engine.product_rule_engine.rules.income_rule import IncomeRule
from domain.risk_analysis.engine.product_rule_engine.rules.marital_status_rule import MaritalStatusRule
from domain.risk_analysis.engine.product_rule_engine.rules.mortgage_rule import MortgageRule
from domain.risk_analysis.engine.product_rule_engine.rules.risk_questions_rule import RiskQuestionsRule
from domain.risk_analysis.engine.product_rule_engine.rules.vehicle_rule import VehicleRule
from domain.risk_analysis.engine.risk_profile_engine import RiskProfileEngine
from domain.risk_analysis.events.risk_profile_evaluated import RiskProfileEvaluated
from infrastructure.domain_event_dispatcher import DomainEventDispatcher
from infrastructure.registration.insurance_applications_repository import InsuranceApplicationsRepository
from infrastructure.registration.insurance_plan_proposals_repository import InsurancePlanProposalsRepository
from infrastructure.risk_analysis.risk_profiles_repository import RiskProfilesRepository

class DependencyInjectionContainer:
    def __init__(self):
        self.build_stateful_objects()
        self.register_domain_events()

    def build_stateful_objects(self):
        self.domain_event_dispatcher = DomainEventDispatcher()
        self.insurance_applications_repository = InsuranceApplicationsRepository(self.domain_event_dispatcher)
        self.risk_profiles_repository = RiskProfilesRepository(self.domain_event_dispatcher)
        self.insurance_plan_proposals_repository = InsurancePlanProposalsRepository(self.domain_event_dispatcher)

    def register_domain_events(self):
        evaluate_risk_profile_handler = EvaluateRiskProfileHandler(self.evaluate_risk_profile_usecase())
        self.domain_event_dispatcher.register(InsuranceApplicationCreated, evaluate_risk_profile_handler)

        self.domain_event_dispatcher.register(RiskProfileEvaluated, self.generate_insurance_plan_proposal_handler())

    def generate_insurance_plan_proposal_handler(self):
        return GenerateInsurancePlanProposalHandler(self.insurance_plan_proposals_repository, InsurancePlanProposalFactory())

    def submit_insurance_application_usecase(self):
        return SubmitInsuranceApplicationUsecase(self.insurance_applications_repository)

    def auto_insurance_rule_engine(self):
        risk_questions_rule = RiskQuestionsRule()
        age_rule = AgeRule()
        income_rule = IncomeRule()

        vehicle_rule = VehicleRule()

        return AutoInsuranceRuleEngine(risk_questions_rule, age_rule, income_rule, vehicle_rule)

    def home_insurance_rule_engine(self):
        risk_questions_rule = RiskQuestionsRule()
        age_rule = AgeRule()
        income_rule = IncomeRule()

        mortgage_rule = MortgageRule()

        return HomeInsuranceRuleEngine(risk_questions_rule, age_rule, income_rule, mortgage_rule)

    def life_insurance_rule_engine(self):
        risk_questions_rule = RiskQuestionsRule()
        age_rule = AgeRule()
        income_rule = IncomeRule()

        dependents_rule = DependentsRule()
        marital_status_rule = MaritalStatusRule()

        return LifeInsuranceRuleEngine(risk_questions_rule, age_rule, income_rule, dependents_rule, marital_status_rule)

    def disability_insurance_rule_engine(self):
        risk_questions_rule = RiskQuestionsRule()
        age_rule = AgeRule()
        income_rule = IncomeRule()

        dependents_rule = DependentsRule()
        marital_status_rule = MaritalStatusRule()

        mortgage_rule = MortgageRule()

        return DisabilityInsuranceRuleEngine(risk_questions_rule, age_rule, income_rule, dependents_rule, marital_status_rule, mortgage_rule)

    def risk_profile_engine(self):
        return RiskProfileEngine(self.life_insurance_rule_engine(),
                self.disability_insurance_rule_engine(),
                self.home_insurance_rule_engine(),
                self.auto_insurance_rule_engine())

    def evaluate_risk_profile_usecase(self):
        return EvaluateRiskProfileUsecase(self.risk_profile_engine(), self.risk_profiles_repository)

