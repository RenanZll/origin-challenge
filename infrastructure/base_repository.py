class BaseRepository:
    def __init__(self, domain_event_dispatcher):
        self.database = []
        self.dispatcher = domain_event_dispatcher

    def persist(self, entity):
        entity.id = len(self.database) + 1
        self.database.append(entity)
        self.dispatcher.dispatch(entity.raise_events())
        return entity

    def last(self):
        return self.database[-1]
