class DomainEventDispatcher:
    def __init__(self):
        self.mapping = {}

    def register(self, domain_event, handler):
        self.mapping[domain_event.name] = handler

    def dispatch(self, events):
        for event in events:
            handler = self.mapping[event.name]
            handler.handle(event)
