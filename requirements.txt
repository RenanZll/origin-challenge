# requirements.txt
# with pinned versions of immediate dependencies
#Flask dependencies
flask==1.0.2
requests==2.20.0
#Test dependencies
pytest
pytest-mock
