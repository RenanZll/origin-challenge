# Origin Challenge

Despite using Python only to play with sklearn, I've never used it to build a structured project, with modules and tests. So, It was a adventure to seek the Python best practices of coding and testing. :)

So, by doing this challenge I tried to apply my knowledge in DDD, layered architecture and Clean Arquicture to segment the project responsability. All the implementation of infrastructure services (repositories, domain events displatcher and DI container) aim to be a simple version of those in the market.

The project was separated in two bounded contexts:
 - **"Registration"**, responsible for receiving insurance application and returning a propposal of insurance plan.
 - **"Risk Analysis"**, responsible for calculating the risk profile by running the rules specified by the challenge.    


## The layered arquicture

**Application** - resides in `application/` folder. All the code that manipulated Domain and Infrastructure objects to execute some actions goes here. The patterns used are `UseCase`, that describe an action that affects the system; and `Handler`, that handle domain events from another context.

**Domain** - resides in `domain/` folder. The business logic and the entities that represents the real complexity of the business goes here. This layer should be as pure as possivel, avoiding as much as possible any type of code that doesn't related with the business.

**Infrastructure** - resides in `infrastructure/` folder. Here is present all the code that supports the persistence, communication and loading of code from the above layers ("Application" and "Domain").

**Presentation** - it is all inside the `./app.py`, idealy should be inside a `presentation/`folder. It is responsible for handling requests/messages from the external world, adapting them to the application layer.

## Dataflow
There is two actions specified in the project.

First, the client submits an insurance application:

**Client** ---submits Application(HTTP)---> **Registration context** ---InsuranceApplicationCreated event---> **Risk Analysis** ---RiskProfileEvaluated event---> **Registration context**(generate a proposal)

Later, it asks for an insurance plan proposal:

**Client** ---asks for Proposal(HTTP)---> **Registration context**(get a Proposal)

## Tests
The tests are divided in integration and unit. The integration tests are applied to usecases, handlers and the api, while the unit tests are applied to the rest of de classes.

## How to run
To build:
```bash
docker-compose build
```

Start the server:
```bash
docker-compose up
```

Run tests:
```bash
docker-compose run web pytest 
```

